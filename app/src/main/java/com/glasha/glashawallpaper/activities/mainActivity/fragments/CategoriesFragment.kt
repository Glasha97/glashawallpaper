package com.glasha.glashawallpaper.activities.mainActivity.fragments

import android.content.Intent
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.activities.imageSortCategoryActivity.ImagesSortCategoryActivity
import com.glasha.glashawallpaper.utils.base.BaseFragment
import com.glasha.glashawallpaper.activities.mainActivity.ViewModelMainActivity
import com.glasha.glashawallpaper.adapters.adapterCategoryRv.AdapterCategoryRV
import com.glasha.glashawallpaper.adapters.adapterCategoryRv.OnClickCategory
import com.glasha.glashawallpaper.models.Category
import com.glasha.glashawallpaper.databinding.FragmentCategoryBinding
import com.glasha.glashawallpaper.utils.Constans


class CategoriesFragment : BaseFragment<FragmentCategoryBinding>(),
    OnClickCategory {


    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var list: ArrayList<Category>
    private var idFocus: Int = 0

    override fun getLayout() = R.layout.fragment_category


    override fun setupBinding(binding: FragmentCategoryBinding) {

        list = arrayListOf()
        viewManager = LinearLayoutManager(this.context!!)
        binding.rvCategory.apply {
            layoutManager = viewManager
        }

        var viewModel = activity?.let { ViewModelProviders.of(it).get(ViewModelMainActivity::class.java) }
        val data = viewModel?.allICategories
        viewModel?.let { onError(it) }
        viewModel?.let { noNetwork(it) }
        data?.observe(this, Observer { arrayList ->
            arrayList.forEach {
                viewModel?.setCountImagesInCategory(it.id)
            }

            val listOfCountCategory = viewModel?.getCountImagesInCategory()
            //  val arrayList=ArrayList(listOfCountCategory)
            listOfCountCategory?.forEach {
            }

            viewAdapter = AdapterCategoryRV(arrayList, this,context!!)
            binding.rvCategory.apply {
                adapter = viewAdapter
            }
        })
    }


    override fun onResume() {
        super.onResume()
        viewManager.scrollToPosition(idFocus)

    }



    private fun onError(viewModel: ViewModelMainActivity) {
        viewModel.onFailureCategories.observe(this, Observer {
            Toast.makeText(activity, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun noNetwork(viewModel: ViewModelMainActivity) {
        viewModel.noNetworkCategoryies.observe(this, Observer {
            Toast.makeText(activity, "Нет соединения с сервером", Toast.LENGTH_LONG).show()
        })
    }

    override fun onClick(adapterPosition: Int, item: Category) {
        var intent = Intent(this.activity, ImagesSortCategoryActivity::class.java)
        idFocus = adapterPosition
        intent.putExtra(Constans.PUT_EXTRA_CATEGORY_ID, item.id)
        intent.putExtra(Constans.PUT_EXTRA_CATEGORY_NAME, item.name)
        startActivity(intent)
    }
}