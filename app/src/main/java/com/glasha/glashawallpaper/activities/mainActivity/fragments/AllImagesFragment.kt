package com.glasha.glashawallpaper.activities.mainActivity.fragments

import android.content.Intent
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.activities.fullImageActivity.FullImageActivity
import com.glasha.glashawallpaper.utils.base.BaseFragment
import com.glasha.glashawallpaper.activities.mainActivity.ViewModelMainActivity
import com.glasha.glashawallpaper.adapters.adapterMainRv.AdapterMainRV
import com.glasha.glashawallpaper.adapters.adapterMainRv.OnClickImage
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.glasha.glashawallpaper.databinding.FragmentImagesAllBinding
import com.glasha.glashawallpaper.utils.Constans
import com.glasha.glashawallpaper.utils.Constans.Companion.PUT_ARGUMENTS_ON_QUERY


class AllImagesFragment : BaseFragment<FragmentImagesAllBinding>(),
    OnClickImage {
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    override fun getLayout() = R.layout.fragment_images_all

    override fun setupBinding(binding: FragmentImagesAllBinding) {
        viewManager = GridLayoutManager(context, 3)
        binding.mainRv.apply {
            layoutManager = viewManager
        }
        var viewModel =
            activity?.let { ViewModelProviders.of(it).get(ViewModelMainActivity::class.java) }
        var data = viewModel?.allImages
        viewModel?.let { onError(it) }
        viewModel?.let { noNetwork(it) }


        val bundle = this.arguments
        if (bundle != null) {

            val search = bundle?.getString(PUT_ARGUMENTS_ON_QUERY, "def_val")
            if (search == "def_val") {
                data?.let { showList(it) }
            } else {
                data?.observe(this, Observer { list ->
                    val result = ArrayList<ImageUrlJava>()
                    list.forEach {
                        if (it.hashTag.contains(search)) {
                            result.add(it)
                        }

                    }

                    viewAdapter = AdapterMainRV(result, this, context!!)
                    binding.mainRv.apply {
                        adapter = viewAdapter
                    }
                })

            }


        } else {
            data?.let { showList(it) }


        }
    }


    private fun showList(data: LiveData<ArrayList<ImageUrlJava>>) {
        data.observe(this, Observer {

            viewAdapter = AdapterMainRV(it, this!!, context!!)
            binding.mainRv.apply {
                adapter = viewAdapter
            }
        })

    }

    private fun onError(viewModel: ViewModelMainActivity) {
        viewModel.onFailureAllImages.observe(this, Observer {
            Toast.makeText(activity, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun noNetwork(viewModel: ViewModelMainActivity) {
        viewModel.noNetworkAllImages.observe(this, Observer {
            Toast.makeText(activity, "Нет соединения с сервером", Toast.LENGTH_LONG).show()
        })
    }

    override fun onClick(item: ImageUrlJava) {
        var intent = Intent(activity, FullImageActivity::class.java)
        intent.putExtra(Constans.PUT_EXTRA_URL_IMAGE, item.url.toString())
        intent.putExtra(Constans.PUT_EXTRA_URL_PATH, item.path.toString())
        intent.putExtra(Constans.PUT_EXTRA_ID_IMAGE, item.id.toString())
        startActivity(intent)
    }

}
