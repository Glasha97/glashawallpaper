package com.glasha.glashawallpaper.activities.settingsAccauntActivity

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.glasha.glashawallpaper.activities.settingsAccauntActivity.interfaces.CallbackGetUser
import com.glasha.glashawallpaper.models.User
import com.glasha.glashawallpaper.utils.FirebaseTools

class ViewModelSettingsAccauntActivity : ViewModel() {
    val repositorySettingsAccauntActivity = RepositorySettingsAccauntActivity()

    val userInfo: MutableLiveData<User> = MutableLiveData()

    fun getUser() {
        repositorySettingsAccauntActivity.getInfoUser(object : CallbackGetUser {
            override fun getUser(user: User) {
                if (userInfo.value == null) {
                    Log.d("cfc", user.email + " vm")

                    userInfo.postValue(user)

                }
            }

        })
    }

    fun updateUserAvatar(userId: String, urlImage: String) {
        var myRef = FirebaseTools.getFireBaseRef("User")
        myRef.child(userId).child("urlImage").setValue(urlImage)


    }
}