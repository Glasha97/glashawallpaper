package com.glasha.glashawallpaper.activities.changeWallpaperTimerActivity

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.work.Data
import androidx.work.PeriodicWorkRequest
import androidx.work.WorkManager
import androidx.work.WorkStatus
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.utils.base.BaseActivity
import com.glasha.glashawallpaper.utils.dataCreatorSpinner.ListsForSpinner
import com.glasha.glashawallpaper.databinding.ActivityTimerWallpaperChangeBinding
import com.glasha.glashawallpaper.utils.CallbackTimePickerOnChanged
import com.glasha.glashawallpaper.utils.Constans.Companion.DEFAULT_WORK_MANAGER_KIND_WALLPAPER_PERIOD_ET_RUS
import com.glasha.glashawallpaper.utils.Constans.Companion.DEFAULT_WORK_MANAGER_KIND_WALLPAPER_RUS
import com.glasha.glashawallpaper.utils.Constans.Companion.DEFAULT_WORK_MANAGER_WHERE_WALLPAPER_RUS
import com.glasha.glashawallpaper.utils.Constans.Companion.SHARED_PREFERENCE_WORK_CHANGE_WALLPAPER
import com.glasha.glashawallpaper.utils.Constans.Companion.SHARED_PREFERENCE_WORK_CHANGE_WALLPAPER_PUT_EXTRA_ID
import com.glasha.glashawallpaper.utils.Constans.Companion.SHARED_PREFERENCE_WORK_CHANGE_WALLPAPER_PUT_EXTRA_PERIOD
import com.glasha.glashawallpaper.utils.Constans.Companion.WORK_MANAGER_PUT_EXTRA_WHERE_WALLPAPER
import com.glasha.glashawallpaper.utils.Constans.Companion.WORK_MANAGER_PUT_EXTRA_WHICH_WALLPAPER
import com.glasha.glashawallpaper.utils.DateEditPicker
import com.glasha.glashawallpaper.activities.changeWallpaperTimerActivity.workManager.ChangeWallpaperWorkManager
import com.glasha.glashawallpaper.utils.Utils
import kotlinx.android.synthetic.main.activity_timer_wallpaper_change.*
import java.util.*
import java.util.concurrent.TimeUnit


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ChangeWallpaperTimerActivity : BaseActivity<ActivityTimerWallpaperChangeBinding>(), View.OnClickListener,
    CallbackTimePickerOnChanged {

    private var changeWallpaperWorkManager: PeriodicWorkRequest? = null
    private lateinit var workId: String
    private var kindWallpaper = DEFAULT_WORK_MANAGER_KIND_WALLPAPER_RUS
    private var whereWallpaper = DEFAULT_WORK_MANAGER_WHERE_WALLPAPER_RUS

    override fun getLayout() = R.layout.activity_timer_wallpaper_change

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun buildToolBar() = binding.changeWallpaperToolbar as Toolbar

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setupBinding(binding: ActivityTimerWallpaperChangeBinding) {
        showArrowBackPress()
        tv_turn_on_off_timer.setOnClickListener(this)
        switch_turnon_off_timer.setOnClickListener(this)

        val period = getPeriodSP()

        dateTV.setText(period)

        val listwhichWallpaper = ListsForSpinner.addListWhichWallpaper(this)
        val listWhereChange = ListsForSpinner.addListWhereWallpaper()

        val adapterWhereWallpaper = ArrayAdapter<String>(this, R.layout.custom_item_spinner, listWhereChange)
        adapterWhereWallpaper.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_where_update_wallpaper.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                whereWallpaper = listWhereChange[p2]
                val isCheck = switch_turnon_off_timer.isChecked
                ischeck(isCheck)

            }

        }
        spinner_where_update_wallpaper.adapter = adapterWhereWallpaper

        val adapterWhichWallpaper = ArrayAdapter<String>(this, R.layout.custom_item_spinner, listwhichWallpaper)
        adapterWhichWallpaper.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_kind_update_wallpaper.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                kindWallpaper = listwhichWallpaper[p2]
                val isCheck = switch_turnon_off_timer.isChecked
                ischeck(isCheck)


            }

        }
        spinner_kind_update_wallpaper.adapter = adapterWhichWallpaper

        workId = getIdWorkSP()
        // WorkManager.getInstance().enqueue(changeWallpaperWorkManager)
        if (workId != null && workId != "null") {
            WorkManager.getInstance().getStatusById(UUID.fromString(workId))
                .observe(this, Observer<WorkStatus> {
                    Log.d("cfc", "obs   " + it.state.toString())
                    switch_turnon_off_timer.isChecked = true
                })
        }

        dateTV.setOnClickListener {
            DateEditPicker(this, dateTV, this).onClick(dateTV)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.tv_turn_on_off_timer -> {
                val isCheck = switch_turnon_off_timer.isChecked
                ischeck(!isCheck)

                if (isCheck) {
                    switch_turnon_off_timer.isChecked = false
                    tv_turn_on_off_timer.text = "Включить"
                } else {
                    switch_turnon_off_timer.isChecked = true
                    tv_turn_on_off_timer.text = "Выключить"
                }
            }
            R.id.switch_turnon_off_timer -> {
                val isCheck = switch_turnon_off_timer.isChecked
                ischeck(isCheck)
                if (isCheck) {
                    tv_turn_on_off_timer.text = "Включить"
                } else {
                    tv_turn_on_off_timer.text = "Выключить"
                }
            }
        }
    }

    private fun ischeck(isCheck: Boolean) {
        if (isCheck) {
            Log.d("cfc", "go")
            stopWork(workId)

            val period = dateTV.text.toString()

            val periodOnlyTime = period.substringAfter(" ")

            val minLong = Utils.convertTimeToMinute(periodOnlyTime)


            val inputData = Data.Builder()
                .putString(WORK_MANAGER_PUT_EXTRA_WHICH_WALLPAPER, kindWallpaper)
                .putString(WORK_MANAGER_PUT_EXTRA_WHERE_WALLPAPER, whereWallpaper).build()
            Log.d("cfc", minLong.toString())
            changeWallpaperWorkManager =
                PeriodicWorkRequest.Builder(
                    ChangeWallpaperWorkManager::class.java,
                    minLong,
                    TimeUnit.MINUTES

                ).setInputData(inputData).build()
            WorkManager.getInstance().enqueue(changeWallpaperWorkManager)

            val workId = changeWallpaperWorkManager?.id.toString()
            setIdWorkSP(workId)


        } else {

            workId = getIdWorkSP()
            stopWork(workId)
        }
    }

    private fun setIdWorkSP(workId: String) {
        val sharedPreference = getSharedPreferences(SHARED_PREFERENCE_WORK_CHANGE_WALLPAPER, Context.MODE_PRIVATE)
        var editor = sharedPreference.edit()
        editor.putString(SHARED_PREFERENCE_WORK_CHANGE_WALLPAPER_PUT_EXTRA_ID, workId)
        editor.apply()
    }

    private fun getIdWorkSP(): String {
        val sharedPreference = getSharedPreferences(SHARED_PREFERENCE_WORK_CHANGE_WALLPAPER, Context.MODE_PRIVATE)
        return sharedPreference.getString(SHARED_PREFERENCE_WORK_CHANGE_WALLPAPER_PUT_EXTRA_ID, "null")
    }

    private fun setPeriodSP(period: String) {
        val sharedPreference = getSharedPreferences(SHARED_PREFERENCE_WORK_CHANGE_WALLPAPER, Context.MODE_PRIVATE)
        var editor = sharedPreference.edit()
        editor.putString(SHARED_PREFERENCE_WORK_CHANGE_WALLPAPER_PUT_EXTRA_PERIOD, period)
        editor.apply()
    }

    private fun getPeriodSP(): String {
        val sharedPreference = getSharedPreferences(SHARED_PREFERENCE_WORK_CHANGE_WALLPAPER, Context.MODE_PRIVATE)
        return sharedPreference.getString(
            SHARED_PREFERENCE_WORK_CHANGE_WALLPAPER_PUT_EXTRA_PERIOD,
            DEFAULT_WORK_MANAGER_KIND_WALLPAPER_PERIOD_ET_RUS
        )
    }


    private fun stopWork(workId: String) {
        if (workId != "null") {
            WorkManager.getInstance().cancelWorkById(UUID.fromString(workId))
        }
    }

    override fun onChanged() {
        setPeriodSP(dateTV.text.toString())
        val isCheck = switch_turnon_off_timer.isChecked
        ischeck(isCheck)

    }

}