package com.glasha.glashawallpaper.activities.mainActivity.interfaces.callBacks

import com.glasha.glashawallpaper.models.Category

interface CallBackOnGetCategories {
    fun onSuccess(result: ArrayList<Category>)
    fun onFailure(message: String)
    fun noNetwork()
}