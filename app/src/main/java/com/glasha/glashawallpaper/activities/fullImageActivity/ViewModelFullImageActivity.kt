package com.glasha.glashawallpaper.activities.fullImageActivity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.glasha.glashawallpaper.activities.fullImageActivity.interfaces.callBacks.CallbackOnCheckLike

class ViewModelFullImageActivity: ViewModel() {
    var repository=FullImageActivityRepository()

    var userLiked: Boolean=false
    var noNetworkAllImages: MutableLiveData<String> = MutableLiveData()

    fun isUserLiked(idImage: String, idUser: String,callbackOnCheckLike: CallbackOnCheckLike) {
        repository.isUserLiked(idImage,idUser,object : CallbackOnCheckLike{
            override fun noNetwork() {
                if(noNetworkAllImages.value!=null){
                    noNetworkAllImages.postValue("noNetwork")
                }
            }

            override fun onReady(isLiked: Boolean) {
             // userLiked=isLiked
                callbackOnCheckLike.onReady(isLiked)
            }
        })


    }
    fun setDeniedUserLike(isLike: Boolean, id: String){
        repository.setDeniedUserLike(isLike,id)

    }
}