package com.glasha.glashawallpaper.activities.imageSortCategoryActivity

import androidx.lifecycle.LiveData
import com.glasha.glashawallpaper.activities.imageSortCategoryActivity.interfaces.CallBackOnGetSortedByCategoryImages
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.glasha.glashawallpaper.utils.FirebaseTools
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class RepositoryImagesSortCategoryActivity {
    fun getAllSortedByCategoryImages(
        idCat: Long,
        callBackOnGetSortedByCategoryImages: CallBackOnGetSortedByCategoryImages
    ) {

        val isConnected = FirebaseTools.isConnected()
        if (isConnected) {


            var result = ArrayList<ImageUrlJava>()

            var firebaseDatabase = FirebaseDatabase.getInstance()
            var databaseReference = firebaseDatabase!!.getReference("urlForImages")
            databaseReference!!.orderByChild("time").addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    result.clear()
                    if (dataSnapshot.exists()) {
                        for (shot in dataSnapshot.children) {

                            var url = shot.getValue(ImageUrlJava::class.java)
                            url?.let {
                                if (idCat == it.categoryId) {
                                    val imageData = ImageUrlJava(
                                        shot.key,
                                        it.url,
                                        it.time,
                                        it.path,
                                        it.categoryId,
                                        it.likes,
                                        it.wholiked,
                                        it.idUser,
                                        it.hashTag
                                    )


                                    result.add(imageData)

                                }
                            }
                        }
                        result.reverse()
                        callBackOnGetSortedByCategoryImages.onSucces(result)
                    }
                }
            })

        }
        else{
            callBackOnGetSortedByCategoryImages.noNetwork()
        }
    }

}