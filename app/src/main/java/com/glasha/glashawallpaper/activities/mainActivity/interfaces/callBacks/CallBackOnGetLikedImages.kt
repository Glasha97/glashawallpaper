package com.glasha.glashawallpaper.activities.mainActivity.interfaces.callBacks

import com.glasha.glashawallpaper.models.ImageUrlJava

interface CallBackOnGetLikedImages {
    fun onSuccess(result: ArrayList<ImageUrlJava>)
    fun onFailure(message: String)
    fun noNetwork()
}