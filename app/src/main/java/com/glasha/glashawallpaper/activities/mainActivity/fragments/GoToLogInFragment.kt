package com.glasha.glashawallpaper.activities.mainActivity.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.activities.logInActivity.LogInActivity
import kotlinx.android.synthetic.main.fragment_login_to_go.view.*

class GoToLogInFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login_to_go, container, false)

        view.go_to_login.setOnClickListener {
            var intent = Intent(activity, LogInActivity::class.java)
            startActivity(intent)

        }
        return view
    }
}