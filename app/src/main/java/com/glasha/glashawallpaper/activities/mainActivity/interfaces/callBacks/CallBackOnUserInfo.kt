package com.glasha.glashawallpaper.activities.mainActivity.interfaces.callBacks

import com.glasha.glashawallpaper.models.User

interface CallBackOnUserInfo {
    fun onSuccess(result: User)
    fun onFailure(message: String)
    fun noNetwork()
}