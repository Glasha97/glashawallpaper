package com.glasha.glashawallpaper.activities.splashActivity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.glasha.glashawallpaper.activities.mainActivity.MainActivity

class SplashActivity :AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent=Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}