package com.glasha.glashawallpaper.activities.mainActivity.fragments

import android.content.Intent
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.activities.changeWallpaperTimerActivity.ChangeWallpaperTimerActivity
import com.glasha.glashawallpaper.activities.logInActivity.LogInActivity
import com.glasha.glashawallpaper.activities.settingsAccauntActivity.SettingsAccauntActivity
import com.glasha.glashawallpaper.utils.base.BaseFragment
import com.glasha.glashawallpaper.databinding.FragmentSettingsBinding
import com.google.firebase.auth.FirebaseAuth
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener

class SettingsFragment : BaseFragment<FragmentSettingsBinding>() {

    private var auth = FirebaseAuth.getInstance()

    override fun getLayout() = R.layout.fragment_settings


    override fun setupBinding(binding: FragmentSettingsBinding) {

        binding.idaccaunt.setOnClickListener {

            if (auth.currentUser != null) {
                var intent = Intent(activity, SettingsAccauntActivity::class.java)
                startActivity(intent)
            } else {

                var intent = Intent(activity, LogInActivity::class.java)
                startActivity(intent)
            }

        }
        binding.idchangeWallpaper.setOnClickListener {
            if (auth.currentUser != null) {
                Dexter.withActivity(activity)
                    .withPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    .withListener(object : PermissionListener {
                        @RequiresApi(Build.VERSION_CODES.N)
                        override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                            var intent = Intent(context, ChangeWallpaperTimerActivity::class.java)
                            startActivity(intent)

                        }

                        override fun onPermissionRationaleShouldBeShown(
                            permission: PermissionRequest?,
                            token: PermissionToken?
                        ) {
                            token?.continuePermissionRequest()
                        }

                        override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                            Toast.makeText(
                                context,
                                "Для работы данной функции необходим доступ к памяти",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }).check()


            } else {
                Toast.makeText(
                    activity,
                    "Для доступа к данной функции необходимо провести аутентификацию",
                    Toast.LENGTH_LONG
                ).show()
                var intent = Intent(activity, LogInActivity::class.java)
                startActivity(intent)
            }

        }
    }


}