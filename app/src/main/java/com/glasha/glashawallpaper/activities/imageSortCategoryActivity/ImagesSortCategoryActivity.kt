package com.glasha.glashawallpaper.activities.imageSortCategoryActivity

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.MenuItem
import android.view.View.VISIBLE
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.activities.fullImageActivity.FullImageActivity
import com.glasha.glashawallpaper.utils.base.BaseActivity
import com.glasha.glashawallpaper.adapters.adapterMainRv.AdapterMainRV
import com.glasha.glashawallpaper.adapters.adapterMainRv.OnClickImage
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.glasha.glashawallpaper.databinding.FragmentImagesAllBinding
import com.glasha.glashawallpaper.utils.Constans

class ImagesSortCategoryActivity : BaseActivity<FragmentImagesAllBinding>(),
    OnClickImage {



    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var list: ArrayList<ImageUrlJava>
    private var categoryId: Long = 0
    private var categoryName: String = ""


    override fun getLayout() = R.layout.fragment_images_all


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun buildToolBar() = binding.toolbarCateg as Toolbar


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setupBinding(binding: FragmentImagesAllBinding) {
        showArrowBackPress()
        binding.toolbarCateg.visibility =VISIBLE

        categoryName = intent.getStringExtra(Constans.PUT_EXTRA_CATEGORY_NAME)
        categoryId = intent.getLongExtra(Constans.PUT_EXTRA_CATEGORY_ID, 0)
        Log.d("cfc", "$categoryId")
        setTitleScreen(categoryName)
        list = arrayListOf()
        viewManager = GridLayoutManager(this, 3)

        var viewModel = ViewModelProviders.of(this).get(ViewModelImageSortCategoryActivity::class.java)
        viewModel.getAllSortedByCategoryImages(categoryId)
        viewModel.allImages.observe(this, Observer {
            viewAdapter = AdapterMainRV(it, this,this)
            binding.mainRv.apply {
                adapter = viewAdapter
                layoutManager = viewManager
            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(item: ImageUrlJava) {
        var intent = Intent(this, FullImageActivity::class.java)
        intent.putExtra(Constans.PUT_EXTRA_URL_IMAGE, item.url.toString())
        intent.putExtra(Constans.PUT_EXTRA_URL_PATH, item.path.toString())
        intent.putExtra(Constans.PUT_EXTRA_ID_IMAGE, item.id.toString())
        startActivity(intent)

    }
}