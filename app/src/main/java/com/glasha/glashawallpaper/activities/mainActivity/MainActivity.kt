package com.glasha.glashawallpaper.activities.mainActivity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProviders
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.activities.loadImageActivity.LoadImageActivity
import com.glasha.glashawallpaper.activities.mainActivity.fragments.*
import com.glasha.glashawallpaper.utils.base.BaseActivity
import com.glasha.glashawallpaper.databinding.ActivityMainBinding
import com.glasha.glashawallpaper.utils.Constans.Companion.PUT_ARGUMENTS_ON_QUERY
import com.glasha.glashawallpaper.utils.FirebaseTools
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemSelectedListener
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<ActivityMainBinding>() {


    private lateinit var fragmentAllImages: AllImagesFragment
    private lateinit var fragmentSettings: SettingsFragment
    private lateinit var fragmentCategories: CategoriesFragment
    private lateinit var likedImagesFragment: LikedImagesFragment
    private lateinit var goToLogInFragment: GoToLogInFragment
    private lateinit var popularImagesFragment: PopularImagesFragment

    private lateinit var viewModelMainActivity: ViewModelMainActivity

    override fun getLayout() = R.layout.activity_main
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
     override fun buildToolBar() = binding.mainToolbar as androidx.appcompat.widget.Toolbar

    // @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setupBinding(binding: ActivityMainBinding) {
        FirebaseApp.initializeApp(this@MainActivity)
        //   showArrowBackPress()
        val toolbar = binding.mainToolbar as androidx.appcompat.widget.Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        binding.bnv.setOnNavigationItemSelectedListener(bottomNav)
        fragmentCategories = CategoriesFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl, fragmentCategories, fragmentCategories.javaClass.simpleName).commit()
        binding.uploadImage.setOnClickListener {
            var intent = Intent(this, LoadImageActivity::class.java)
            startActivity(intent)
        }
        startViewModel()

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_search) {
            val searchView = item.actionView as SearchView
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {

                    Handler().postDelayed({
                        fragmentAllImages =
                            AllImagesFragment()
                        val bundle = Bundle()
                        bundle.putString(PUT_ARGUMENTS_ON_QUERY, query)
                        fragmentAllImages.arguments = bundle
                        supportFragmentManager.beginTransaction().setCustomAnimations(
                            R.anim.animate_inside_fragment_from_bottom,
                            R.anim.animate_outside_fragment_from_bottom
                        )
                            .replace(R.id.fl, fragmentAllImages).commit()

                    }, 250)

                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return true
                }

            })

        } else if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("RestrictedApi")
    override fun onResume() {
        super.onResume()
        val user = FirebaseTools.getUser()
        user?.let {
            upload_image.visibility = FloatingActionButton.VISIBLE
            viewModelMainActivity.getLikedImages()

        }
    }

    private fun startViewModel() {
        viewModelMainActivity = ViewModelProviders.of(this).get(ViewModelMainActivity::class.java)
        viewModelMainActivity.getAllImages()
        viewModelMainActivity.getCategories()
    }

    private val bottomNav = OnNavigationItemSelectedListener {
        when (it.itemId) {
            R.id.idAll -> {
                fragmentAllImages = AllImagesFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fl, fragmentAllImages, fragmentAllImages.javaClass.simpleName).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.idCategories -> {
                fragmentCategories = CategoriesFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fl, fragmentCategories, fragmentCategories.javaClass.simpleName).commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.idLike -> {
                val id = FirebaseTools.getUIDUser()
                if (id == "null") {
                    goToLogInFragment = GoToLogInFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.fl, goToLogInFragment, goToLogInFragment.javaClass.simpleName)
                        .commit()

                } else {
                    likedImagesFragment =
                        LikedImagesFragment()
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.fl, likedImagesFragment, likedImagesFragment.javaClass.simpleName).commit()

                }
                return@OnNavigationItemSelectedListener true
            }
            R.id.idPopular -> {
                popularImagesFragment =
                    PopularImagesFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fl, popularImagesFragment, popularImagesFragment.javaClass.simpleName).commit()


                return@OnNavigationItemSelectedListener true
            }
            R.id.idAcc -> {
                fragmentSettings = SettingsFragment()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fl, fragmentSettings, fragmentSettings.javaClass.simpleName).commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }
}
