package com.glasha.glashawallpaper.activities.loadImageActivity.interfaces.callbacks

interface CallBackOnLoadedImage {
    fun onLoaded()
    fun onFailure(message:String)
    fun noNetwork()
}