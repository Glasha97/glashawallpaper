package com.glasha.glashawallpaper.activities.imageSortCategoryActivity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.glasha.glashawallpaper.activities.imageSortCategoryActivity.interfaces.CallBackOnGetSortedByCategoryImages
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class ViewModelImageSortCategoryActivity : ViewModel() {
     var allImages: MutableLiveData<ArrayList<ImageUrlJava>> = MutableLiveData()
    private val repositoryImagesSortCategoryActivity=RepositoryImagesSortCategoryActivity()



    fun getAllSortedByCategoryImages(idCat: Long): LiveData<ArrayList<ImageUrlJava>> {
        if (allImages.value == null) {
repositoryImagesSortCategoryActivity.getAllSortedByCategoryImages(idCat,object : CallBackOnGetSortedByCategoryImages{
    override fun onSucces(list: ArrayList<ImageUrlJava>) {
        allImages.postValue(list)

    }

    override fun noNetwork() {
    }

})

        }

        return allImages
    }

}