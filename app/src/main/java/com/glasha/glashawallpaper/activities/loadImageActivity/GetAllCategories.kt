package com.glasha.glashawallpaper.activities.loadImageActivity

import com.glasha.glashawallpaper.models.Category
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class GetAllCategories(private val callbak: CallbackGetCategories) {

        fun get() {
            var result = ArrayList<Category>()
            var firebaseDatabase = FirebaseDatabase.getInstance()
            var databaseReference = firebaseDatabase!!.getReference("Category")
            databaseReference!!.orderByChild("name").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    result.clear()
                    if (dataSnapshot.exists()) {
                        for (shot in dataSnapshot.children) {

                            var url = shot.getValue(Category::class.java)
                            url?.let {
                                result.add(it)


                            }
                        }

                    }
                    callbak.getCategories(result)

                }
            })

        }


    interface CallbackGetCategories{
        fun getCategories(result:ArrayList<Category>)
    }

}