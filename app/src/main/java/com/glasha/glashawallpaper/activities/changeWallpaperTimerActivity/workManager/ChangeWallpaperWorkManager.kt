package com.glasha.glashawallpaper.activities.changeWallpaperTimerActivity.workManager

import android.app.WallpaperManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.provider.MediaStore
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.work.Worker
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.glasha.glashawallpaper.utils.Constans
import com.glasha.glashawallpaper.utils.Constans.Companion.DEFAULT_WORK_MANAGER_KIND_WALLPAPER_RUS
import com.glasha.glashawallpaper.utils.Constans.Companion.DEFAULT_WORK_MANAGER_WHERE_WALLPAPER_RUS
import com.glasha.glashawallpaper.utils.Constans.Companion.WORK_MANAGER_WHERE_WALLPAPER_ALL_RUS
import com.glasha.glashawallpaper.utils.Constans.Companion.WORK_MANAGER_WHERE_WALLPAPER_LOCKSCREEN_RUS
import com.glasha.glashawallpaper.utils.FirebaseTools
import com.glasha.glashawallpaper.utils.LoadImageUtils
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ChangeWallpaperWorkManager : Worker() {
    private var result = ArrayList<ImageUrlJava>()
    private var loadImageUtils = LoadImageUtils(applicationContext)

    override fun doWork(): WorkerResult {
        Log.d("cfc", "work")


        val which = inputData.getString(
            Constans.WORK_MANAGER_PUT_EXTRA_WHICH_WALLPAPER,
            DEFAULT_WORK_MANAGER_KIND_WALLPAPER_RUS
        )
        val where = inputData.getString(
            Constans.WORK_MANAGER_PUT_EXTRA_WHERE_WALLPAPER,
            DEFAULT_WORK_MANAGER_WHERE_WALLPAPER_RUS
        )

        if (which == DEFAULT_WORK_MANAGER_KIND_WALLPAPER_RUS) {

            var firebaseDatabase = FirebaseDatabase.getInstance()
            var databaseReference = firebaseDatabase!!.getReference("urlForImages")
            databaseReference!!.orderByChild("time").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {

                    result.clear()
                    if (dataSnapshot.exists()) {
                        for (shot in dataSnapshot.children) {

                            shot.getValue(ImageUrlJava::class.java).apply {
                                val imageData = ImageUrlJava(
                                    shot.key,
                                    this?.url,
                                    this?.time,
                                    this?.path,
                                    this?.categoryId!!,
                                    this.likes,
                                    this.wholiked,
                                    this.idUser,
                                    this.hashTag
                                )
                                result.add(imageData)
                            }

                        }
                    }
                    whereChange(where, result)
                }
            })


        } else {
            val uid = FirebaseTools.getUIDUser()
            val ref = FirebaseTools.getFireBaseRef(Constans.URL_FOR_iMAGE)

            ref.orderByChild("time").addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    result.clear()

                    if (dataSnapshot.exists()) {
                        for (shot in dataSnapshot.children) {

                            shot.getValue(ImageUrlJava::class.java).apply {
                                this?.wholiked?.get(uid)?.let {
                                    val imageData = ImageUrlJava(
                                        shot.key,
                                        this?.url,
                                        this?.time,
                                        this?.path,
                                        this?.categoryId!!,
                                        this.likes,
                                        this.wholiked,
                                        this.idUser,
                                        this.hashTag
                                    )
                                    result.add(imageData)
                                }
                            }
                        }
                    }
                    whereChange(where, result)

                }
            })


        }
        return WorkerResult.SUCCESS
    }

    private fun whereChange(where: String, list: ArrayList<ImageUrlJava>) {
        val size = list.size
        if (size != 0) {
            val someInt = (1 until size).shuffled().first()
            val getImage = list[someInt]

            when (where) {
                DEFAULT_WORK_MANAGER_WHERE_WALLPAPER_RUS -> {
                    //settings.loadAndChangeWallpaper(getImage.url, WallpaperManager.FLAG_SYSTEM)
                    // val sett=Settings(applicationContext)
                    // sett.loadAndChangeWallpaper(getImage.url, WallpaperManager.FLAG_SYSTEM)

                    Glide.with(applicationContext)
                        .asBitmap()
                        .load(getImage.url)
                        .into(object : CustomTarget<Bitmap>() {
                            override fun onLoadFailed(errorDrawable: Drawable?) {
                                Log.d("cfc", "onLoadFailed  ${errorDrawable.toString()}")

                            }

                            @RequiresApi(Build.VERSION_CODES.N)
                            override fun onResourceReady(
                                resource: Bitmap,
                                transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                            ) {

                                MediaStore.Images.Media.insertImage(
                                    applicationContext.contentResolver,
                                    resource,
                                    "",
                                    ""
                                )
                                GlobalScope.launch {
                                    var wallpaper: WallpaperManager = WallpaperManager.getInstance(applicationContext)
                                    wallpaper.setBitmap(resource, null, true, WallpaperManager.FLAG_SYSTEM)
                                }
                            }

                            override fun onLoadCleared(placeholder: Drawable?) {

                            }
                        })

                }
                WORK_MANAGER_WHERE_WALLPAPER_LOCKSCREEN_RUS -> {

                    loadImageUtils.loadAndChangeWallpaper(getImage.url, WallpaperManager.FLAG_LOCK)

                }
                WORK_MANAGER_WHERE_WALLPAPER_ALL_RUS -> {

                    loadImageUtils.loadAndChangeWallpaper(getImage.url)
                }
            }
        }

    }
}