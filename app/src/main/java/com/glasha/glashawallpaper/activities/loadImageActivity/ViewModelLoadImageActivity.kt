package com.glasha.glashawallpaper.activities.loadImageActivity

import android.net.Uri
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.activities.loadImageActivity.interfaces.callbacks.CallBackOnLoadedImage
import com.glasha.glashawallpaper.models.Category
import com.glasha.glashawallpaper.utils.IdResourceString
import com.glasha.glashawallpaper.utils.ResourceString
import com.glasha.glashawallpaper.utils.TextResourceString
import com.glasha.glashawallpaper.utils.ToastForViewModel

class ViewModelLoadImageActivity : ViewModel(), CallBackOnLoadedImage {
    var showPB = ObservableBoolean(false)

    internal val toastMessage = ToastForViewModel<ResourceString>()

    fun ToastOnSuccess() {
        toastMessage.value = IdResourceString(R.string.imageLoaded)
    }

    fun ToastOnFailure(message: String) {
        toastMessage.value = TextResourceString(message)
    }


    val repositoryLoadImageActivity = RepositoryLoadImageActivity()

    fun loadImage(
        uri: Uri,
        path: String,
        hashTag: String,
        cat: Category

    ) {
        showPB.set(true)
        repositoryLoadImageActivity.setOnLoadedImage(this)
        repositoryLoadImageActivity.loadImageToStorage(uri, path, hashTag, cat)
    }

    override fun onLoaded() {
        ToastOnSuccess()
        showPB.set(false)

    }

    override fun onFailure(message: String) {
        ToastOnFailure(message)
        showPB.set(false)

    }

    override fun noNetwork() {
        showPB.set(false)

    }
}