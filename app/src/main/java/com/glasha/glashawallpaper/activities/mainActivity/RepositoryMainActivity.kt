package com.glasha.glashawallpaper.activities.mainActivity

import com.glasha.glashawallpaper.activities.mainActivity.interfaces.callBacks.CallBackOnGetAllImages
import com.glasha.glashawallpaper.activities.mainActivity.interfaces.callBacks.CallBackOnGetCategories
import com.glasha.glashawallpaper.activities.mainActivity.interfaces.callBacks.CallBackOnGetLikedImages
import com.glasha.glashawallpaper.models.Category
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.glasha.glashawallpaper.utils.Constans
import com.glasha.glashawallpaper.utils.FirebaseTools
import com.google.firebase.database.*

class RepositoryMainActivity {

    fun getAllImages(callBackOnGetAllImages: CallBackOnGetAllImages) {
        val isConnected = FirebaseTools.isConnected()
        if (isConnected) {
            var result = ArrayList<ImageUrlJava>()
            val databaseReference = FirebaseTools.getFireBaseRef(Constans.URL_FOR_iMAGE)
            databaseReference!!.orderByChild("time").addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    callBackOnGetAllImages.onFailure(p0.message)

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    result.clear()
                    if (dataSnapshot.exists()) {
                        for (shot in dataSnapshot.children) {

                            shot.getValue(ImageUrlJava::class.java).apply {
                                val imageData = ImageUrlJava(
                                    shot.key,
                                    this?.url,
                                    this?.time,
                                    this?.path,
                                    this?.categoryId!!,
                                    this.likes,
                                    this.wholiked,
                                    this.idUser,
                                    this.hashTag
                                )
                                result.add(imageData)
                            }

                        }
                        result.reverse()
                        callBackOnGetAllImages.onSuccess(result)

                    }
                }
            })
        } else {
            callBackOnGetAllImages.noNetwork()
        }
    }


    fun getAllCategories(callBackOnGetCategories: CallBackOnGetCategories) {
        val isConnected = FirebaseTools.isConnected()
        if (isConnected) {
            var result = ArrayList<Category>()

            val databaseReference = FirebaseTools.getFireBaseRef("Category")
            databaseReference!!.orderByChild("name").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    callBackOnGetCategories.onFailure(p0.message)
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    result.clear()
                    if (dataSnapshot.exists()) {
                        for (shot in dataSnapshot.children) {

                            var url = shot.getValue(Category::class.java)
                            url?.let {
                                result.add(it)


                            }
                        }

                    }
                    callBackOnGetCategories.onSuccess(result)
                }
            })
        } else {
            callBackOnGetCategories.noNetwork()
        }
    }


    fun getLikedImages(callBackOnGetLikedImages: CallBackOnGetLikedImages) {
        val isConnected = FirebaseTools.isConnected()
        if (isConnected) {
            val uid = FirebaseTools.getUIDUser()
            val ref = FirebaseTools.getFireBaseRef(Constans.URL_FOR_iMAGE)
            var result = ArrayList<ImageUrlJava>()


            ref.orderByChild("time").addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    callBackOnGetLikedImages.onFailure(p0.message)
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    result.clear()

                    if (dataSnapshot.exists()) {
                        for (shot in dataSnapshot.children) {

                            shot.getValue(ImageUrlJava::class.java).apply {
                                this?.wholiked?.get(uid)?.let {
                                    val imageData = ImageUrlJava(
                                        shot.key,
                                        this?.url,
                                        this?.time,
                                        this?.path,
                                        this?.categoryId!!,
                                        this.likes,
                                        this.wholiked,
                                        this.idUser,
                                        this.hashTag
                                    )
                                    result.add(imageData)
                                }
                            }
                        }
                        result.reverse()
                        callBackOnGetLikedImages.onSuccess(result)
                    }
                }
            })
        } else {
            callBackOnGetLikedImages.noNetwork()
        }

    }


//todo rep for settAcc Activity

//    fun getIUserInfo(callBackOnUserInfo: CallBackOnUserInfo) {
//        val isConnected = FirebaseTools.isConnected()
//        if (isConnected) {
//            val id = FirebaseTools.getUIDUser()
//            var database = FirebaseDatabase.getInstance()
//            var myRef = database.getReference("User")
//            myRef.child(id).addListenerForSingleValueEvent(object : ValueEventListener {
//                override fun onCancelled(p0: DatabaseError) {
//                    callBackOnUserInfo.onFailureCategories(p0.message)
//                }
//
//                override fun onDataChange(snapShot: DataSnapshot) {
//                    if (snapShot.exists()) {
//                        var user = snapShot.getValue(User::class.java)
//                        user?.let {
//                            callBackOnUserInfo.onSuccess(user)
//                        } ?: run {
//                            callBackOnUserInfo.onFailureCategories("error")
//                        }
//                    }
//                }
//
//            })
//        } else {
//            callBackOnUserInfo.noNetworkCategoryies()
//        }
//
//
//    }


    //todo rep for categoryActivity


//    fun getAllImagesFilteredByCategories(
//        idCat: Long,
//        callBackOnGetAllImagesFilteredByCategies: CallBackOnGetAllImages
//    ) {
//        val isConnected = FirebaseTools.isConnected()
//        if (isConnected) {
//            var result = ArrayList<ImageUrlJava>()
//            val databaseReference = FirebaseTools.getFireBaseRef(Constans.URL_FOR_iMAGE)
//            databaseReference!!.orderByChild("time").addValueEventListener(object : ValueEventListener {
//                override fun onCancelled(p0: DatabaseError) {
//                    callBackOnGetAllImagesFilteredByCategies.onFailureCategories(p0.message)
//
//                }
//
//                override fun onDataChange(dataSnapshot: DataSnapshot) {
//                    result.clear()
//                    if (dataSnapshot.exists()) {
//                        for (shot in dataSnapshot.children) {
//
//                            var url = shot.getValue(ImageUrlJava::class.java)
//                            url?.let {
//                                if (idCat == it.categoryId) {
//                                    val imageData = ImageUrlJava(
//                                        shot.key,
//                                        it.url,
//                                        it.time,
//                                        it.path,
//                                        it.categoryId,
//                                        it.likes,
//                                        it.wholiked,
//                                        it.idUser,
//                                        it.hashTag
//                                    )
//
//
//                                    result.add(imageData)
//
//                                }
//                            }
//                        }
//                        result.reverse()
//                        callBackOnGetAllImagesFilteredByCategies.onSuccess(result)
//                    }
//                }
//            })
//        } else {
//            callBackOnGetAllImagesFilteredByCategies.noNetworkCategoryies()
//        }
//    }


    //todo rep for LoadImageTOFB


    //    fun loadImageToFB(imageInfo: ImageUrlJava, callBackOnLoadImageToFB: CallBackOnLoadImageToFB) {
//        val isConnected = FirebaseTools.isConnected()
//        if (isConnected) {
//            val ref = FirebaseTools.getFireBaseRef(Constans.URL_FOR_iMAGE).push()
//            ref.setValue(imageInfo)
//            ref.child("wholiked").child("idusernotnull").setValue("iduser123")
//            callBackOnLoadImageToFB.onSuccess()
//
//        } else {
//            callBackOnLoadImageToFB.noNetworkCategoryies()
//        }
//
//
//    }


    //todo rep for LogInActivty


    //    fun RegistrateOrUpdateUser(user: User, callBackOnRegUpdateUser: CallBackOnRegUpdateUser) {
//        val isConnected = FirebaseTools.isConnected()
//        if (isConnected) {
//            val myRef = FirebaseTools.getFireBaseRef("User")
//
//            if (user.urlImage == "") {
//                callBackOnRegUpdateUser.onSuccess()
//                myRef.child("User").child(user.id.toString()).child("name").setValue(user.name)
//
//            } else {
//                myRef.child("User").child(user.id.toString()).setValue(user)
//                callBackOnRegUpdateUser.onSuccess()
//            }
//
//        } else {
//            callBackOnRegUpdateUser.noNetworkCategoryies()
//        }
//
//    }

    //todo rep for FullImageActivity


//    fun setDeniedLikeImage(isLike: Boolean, id: String, callBackOnSetDeniedLikeImage: CallBackOnSetDeniedLikeImage) {
//        val isConnected = FirebaseTools.isConnected()
//        if (isConnected) {
//            val user = FirebaseTools.getUser()
//            val ref = FirebaseTools.getFireBaseRef(Constans.URL_FOR_iMAGE).child(id)
//            ref.runTransaction(object : Transaction.Handler {
//                override fun onComplete(p0: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {
//
//                }
//
//                @SuppressLint("RestrictedApi")
//                override fun doTransaction(mutableData: MutableData): Transaction.Result {
//                    var data = mutableData.getValue(ImageUrlJava::class.java)
//                    if (isLike) {
//                        mutableData.child("likes").value = data?.likes!! - 1
//                        mutableData.child("wholiked").child(user?.uid.toString()).value = null
//
//                    } else {
//                        mutableData.child("likes").value = data?.likes!! + 1
//                        mutableData.child("wholiked").child(user?.uid.toString()).value = user?.uid.toString()
//                    }
//                    callBackOnSetDeniedLikeImage.onSuccess()
//                    return Transaction.success(mutableData)
//                }
//            }
//
//
//            )
//        } else {
//            callBackOnSetDeniedLikeImage.noNetworkCategoryies()
//
//        }
//
//
//    }
//
//    fun isUserLiked(idImage: String, idUser: String, callBack: CallbackOnCheckLike) {
//        var islikeBoolean = false
//        var firebaseDatabase = FirebaseDatabase.getInstance()
//        var ref = firebaseDatabase!!.getReference(Constans.URL_FOR_iMAGE).child(idImage)
//        Log.d("cfc", ref.toString())
//        ref.addListenerForSingleValueEvent(object : ValueEventListener {
//            override fun onCancelled(p0: DatabaseError) {
//                callBack.onReady(false)
//
//            }
//
//            @SuppressLint("RestrictedApi")
//            override fun onDataChange(dataSnapshot: DataSnapshot) {
//
//                if (dataSnapshot.exists()) {
//
//
//                    val data = dataSnapshot.getValue(ImageUrlJava::class.java)
//                    data?.wholiked?.get(idUser)?.let {
//                        islikeBoolean = true
//                    }
//                }
//                callBack.onReady(islikeBoolean)
//            }
//        })
//
//    }

}