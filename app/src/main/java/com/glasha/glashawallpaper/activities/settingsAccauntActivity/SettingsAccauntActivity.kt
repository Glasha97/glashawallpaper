package com.glasha.glashawallpaper.activities.settingsAccauntActivity

import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.KeyEvent
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.activities.mainActivity.MainActivity
import com.glasha.glashawallpaper.databinding.ActivityAccauntSettingsBinding
import com.glasha.glashawallpaper.models.User
import com.glasha.glashawallpaper.utils.Constans.Companion.START_ACTIVITY_RESULT_CHOOSE_PHOTO
import com.glasha.glashawallpaper.utils.FirebaseTools
import com.glasha.glashawallpaper.utils.FirebaseTools.Companion.getAuthInstance
import com.glasha.glashawallpaper.utils.GoogleAuth
import com.glasha.glashawallpaper.utils.PhotoUtils
import com.glasha.glashawallpaper.utils.base.BaseActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_accaunt_settings.*
import java.io.File
import java.io.IOException


class SettingsAccauntActivity : BaseActivity<ActivityAccauntSettingsBinding>() {
    private val mAuth = getAuthInstance()
    private lateinit var viewModelSettingsAccauntActivity: ViewModelSettingsAccauntActivity
    private var photoUtils= PhotoUtils(this,this)
    val googleAuth = GoogleAuth(this, this, getAuthInstance())




    override fun getLayout() = R.layout.activity_accaunt_settings


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun buildToolBar() = binding.ToolbarSettings as Toolbar


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setupBinding(binding: ActivityAccauntSettingsBinding) {
        showArrowBackPress()
        viewModelSettingsAccauntActivity = ViewModelProviders.of(this).get(ViewModelSettingsAccauntActivity::class.java)
        viewModelSettingsAccauntActivity.getUser()
        viewModelSettingsAccauntActivity.userInfo.observe(this, Observer {
            editText.setText(it.name.toString())
            edEmail.setText(it.email.toString())
            it.urlImage?.let { circleImageView ->
                Picasso.with(this)
                    .load(it.urlImage)
                    .fit().centerCrop()
                    .into(imageAcc)

            }
        })

        logout_btn.setOnClickListener {
            FirebaseTools.logout()
            mAuth?.signOut()
            FirebaseTools.googleLogout(this)
        }
        editText.setOnKeyListener { view, keyCode, event ->
            if (keyCode == EditorInfo.IME_ACTION_SEARCH ||
                keyCode == EditorInfo.IME_ACTION_DONE ||
                event.action == KeyEvent.ACTION_DOWN &&
                event.keyCode == KeyEvent.KEYCODE_ENTER
            ) {
                if (!event.isShiftPressed) {
                   // googleAuth.showPB.set(true)
                    // binding.pbChangeLogin.visibility=ProgressBar.VISIBLE
                    var newLogin = editText.text.toString()
                    var userData = User(FirebaseTools.getUIDUser(), newLogin)
                    // registrataUserProvider =
                    //  RegistrateOrUpdateUserProvider(userData)
                    binding.googleAuthSettings = googleAuth

                    googleAuth.registrateOrUpdateUser(userData)

                }
            }
            return@setOnKeyListener false
        }

        imageAcc.setOnClickListener {

            try {
                val photoFile: File? = photoUtils.createTempImageFile(this)
                photoFile!!.absolutePath
                val photoURI = FileProvider.getUriForFile(this, "com.glasha.glashawallpaper.fileprovider", photoFile)
                startActivityForResult(photoUtils.intentToPhoto(this, photoURI), START_ACTIVITY_RESULT_CHOOSE_PHOTO)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBack(this@SettingsAccauntActivity)
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == START_ACTIVITY_RESULT_CHOOSE_PHOTO) {

            if (data?.data != null) {
               val uri= photoUtils.changePhotoUser(data!!, imageAcc)
                viewModelSettingsAccauntActivity.updateUserAvatar(FirebaseTools.getUIDUser(),uri.toString())

            }
        }
    }

    private fun onBack(context: Context) {
        var intent = Intent(context, MainActivity::class.java)
        startActivity(intent)
    }

    override fun onBackPressed() {
        onBack(this@SettingsAccauntActivity)

    }
}