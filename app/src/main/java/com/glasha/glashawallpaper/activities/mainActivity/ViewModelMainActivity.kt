package com.glasha.glashawallpaper.activities.mainActivity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.glasha.glashawallpaper.activities.mainActivity.interfaces.callBacks.CallBackOnGetAllImages
import com.glasha.glashawallpaper.activities.mainActivity.interfaces.callBacks.CallBackOnGetCategories
import com.glasha.glashawallpaper.activities.mainActivity.interfaces.callBacks.CallBackOnGetLikedImages
import com.glasha.glashawallpaper.models.Category
import com.glasha.glashawallpaper.models.ImageUrlJava

class ViewModelMainActivity : ViewModel() {
    var repository = RepositoryMainActivity()

    private var isLoad = false
    var allImages: MutableLiveData<ArrayList<ImageUrlJava>> = MutableLiveData()
    var onFailureAllImages: MutableLiveData<String> = MutableLiveData()
    var noNetworkAllImages: MutableLiveData<String> = MutableLiveData()


    var allICategories: MutableLiveData<ArrayList<Category>> = MutableLiveData()
    var noNetworkCategoryies: MutableLiveData<String> = MutableLiveData()
    var onFailureCategories: MutableLiveData<String> = MutableLiveData()
    private var countImagesInCategory = ArrayList<Long>()

    var likedImages: MutableLiveData<ArrayList<ImageUrlJava>> = MutableLiveData()
    var onFailureLikedImages: MutableLiveData<String> = MutableLiveData()
    var noNetworkLikedImages: MutableLiveData<String> = MutableLiveData()



    fun getCountImagesInCategory(): ArrayList<Long> {
        return countImagesInCategory
    }


    fun setCountImagesInCategory(countImagesInCategoryItem: Long) {
        countImagesInCategory.add(countImagesInCategoryItem)
    }


    fun getAllImages() {
        if (allImages.value == null) {


            isLoad = true
            repository.getAllImages(object : CallBackOnGetAllImages {
                override fun onSuccess(result: ArrayList<ImageUrlJava>) {
                    allImages.postValue(result)
                    isLoad = false
                }

                override fun onFailure(message: String) {
                    onFailureAllImages.postValue(message)
                    isLoad = false
                }

                override fun noNetwork() {
                    noNetworkAllImages.postValue("noNetwork")
                    isLoad = false
                }

            })
        }
    }

    fun getCategories() {
        if (allICategories.value == null) {


            repository.getAllCategories(object : CallBackOnGetCategories {
                override fun onSuccess(result: ArrayList<Category>) {
                    allICategories.postValue(result)
                    isLoad = false

                }

                override fun onFailure(message: String) {
                    onFailureCategories.postValue(message)
                    isLoad = false

                }

                override fun noNetwork() {
                    isLoad = false
                    noNetworkCategoryies.postValue("noNetworkCategoryies")

                }

            })
        }
    }

    fun getLikedImages(){
        if(likedImages.value==null){
            repository.getLikedImages(object : CallBackOnGetLikedImages{
                override fun onSuccess(result: ArrayList<ImageUrlJava>) {
                    likedImages.postValue(result)

                }

                override fun onFailure(message: String) {
                    onFailureLikedImages.postValue(message)
                }

                override fun noNetwork() {
                    noNetworkLikedImages.postValue("noNetworkLikedImages")
                }

            })
        }
    }

}