package com.glasha.glashawallpaper.activities.fullImageActivity


import android.app.WallpaperManager
import android.os.Build
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProviders
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.utils.base.BaseActivity
import com.glasha.glashawallpaper.databinding.ActivityImageFullBinding
import com.glasha.glashawallpaper.activities.fullImageActivity.interfaces.callBacks.CallbackOnCheckLike
import com.glasha.glashawallpaper.utils.Constans
import com.glasha.glashawallpaper.utils.FirebaseTools
import com.glasha.glashawallpaper.utils.FirebaseTools.Companion.getUIDUser
import com.glasha.glashawallpaper.utils.LoadImageUtils
import com.squareup.picasso.Picasso

class FullImageActivity : BaseActivity<ActivityImageFullBinding>(), View.OnCreateContextMenuListener{


    private lateinit var path: String
    private lateinit var url: String
    private var loadImageUtils=LoadImageUtils(this,this)
    private lateinit var id: String
    private var viewModel: ViewModelFullImageActivity? = null

    override fun getLayout() = R.layout.activity_image_full

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun buildToolBar() = binding.toolbarFullImageActivity as Toolbar

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setupBinding(binding: ActivityImageFullBinding) {
        showArrowBackPress()
        val user = FirebaseTools.getUser()
        user?.let {
            binding.likeImage.visibility = ImageView.VISIBLE
        }
        getExtras()
        viewModel = ViewModelProviders.of(this).get(ViewModelFullImageActivity::class.java)
        viewModel?.isUserLiked(id, getUIDUser(), object : CallbackOnCheckLike {
            override fun onReady(isLiked: Boolean) {
                if (isLiked) {
                    binding.likeImage.setBackgroundResource(R.drawable.ic_favorite_red_24dp)
                } else {
                    binding.likeImage.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp)
                }
            }

            override fun noNetwork() {
            }

        })

        Picasso.with(this)
            .load(url)
            .fit().centerCrop()
            .into(binding.fullIV)


        binding.likeImage.setOnClickListener {

            viewModel?.isUserLiked(id, getUIDUser(), object : CallbackOnCheckLike {
                override fun onReady(isLiked: Boolean) {
                    viewModel!!.setDeniedUserLike(isLiked, id)
                    if (isLiked) {
                        binding.likeImage.setBackgroundResource(R.drawable.ic_favorite_border_black_24dp)
                    } else {
                        binding.likeImage.setBackgroundResource(R.drawable.ic_favorite_red_24dp)

                    }
                }

                override fun noNetwork() {
                }

            })

        }
        binding.downloadImage.setOnCreateContextMenuListener(this)
        binding.downloadImage.setOnClickListener {
            it.showContextMenu()
        }
    }


    private val onEditMenu = MenuItem.OnMenuItemClickListener { item ->
        when (item.itemId) {
            1 -> {
                loadImageUtils.loadImageToStorage(url)
            }
            2 -> {
                //lockscreen
                loadImageUtils.loadAndChangeWallpaper(url, WallpaperManager.FLAG_LOCK)
            }
            3 -> {
                //homescreen
                loadImageUtils.loadAndChangeWallpaper(url, WallpaperManager.FLAG_SYSTEM)
            }
            4 -> {
                loadImageUtils.loadAndChangeWallpaper(url)
            }

        }
        true

    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val download = menu?.add(Menu.NONE, 1, 1, "Скачать")
        download?.setOnMenuItemClickListener(onEditMenu)
        val lockscreen = menu?.add(Menu.NONE, 2, 2, "Экран блокировки")
        lockscreen?.setOnMenuItemClickListener(onEditMenu)
        val homescreen = menu?.add(Menu.NONE, 3, 3, "Домашний экран")
        homescreen?.setOnMenuItemClickListener(onEditMenu)

        val all = menu?.add(Menu.NONE, 4, 4, "Экран блокировки и домашний экран")
        all?.setOnMenuItemClickListener(onEditMenu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getExtras() {
        url = intent.getStringExtra(Constans.PUT_EXTRA_URL_IMAGE)
        id = intent.getStringExtra(Constans.PUT_EXTRA_ID_IMAGE)
        path = intent.getStringExtra(Constans.PUT_EXTRA_URL_PATH)
    }




}