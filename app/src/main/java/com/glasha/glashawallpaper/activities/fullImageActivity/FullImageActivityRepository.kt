package com.glasha.glashawallpaper.activities.fullImageActivity

import android.annotation.SuppressLint
import android.util.Log
import com.glasha.glashawallpaper.activities.fullImageActivity.interfaces.callBacks.CallbackOnCheckLike
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.glasha.glashawallpaper.utils.Constans
import com.glasha.glashawallpaper.utils.FirebaseTools
import com.google.firebase.database.*

class FullImageActivityRepository {

    fun isUserLiked(idImage: String, idUser: String, callBack: CallbackOnCheckLike) {
        var islikeBoolean = false
        var firebaseDatabase = FirebaseDatabase.getInstance()
        var ref = firebaseDatabase!!.getReference(Constans.URL_FOR_iMAGE).child(idImage)
        Log.d("cfc",ref.toString())
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                callBack.onReady(false)

            }

            @SuppressLint("RestrictedApi")
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                if (dataSnapshot.exists()) {


                    val data= dataSnapshot.getValue(ImageUrlJava::class.java)
                    data?.wholiked?.get(idUser)?.let {
                        islikeBoolean = true
                    }
                }
                callBack.onReady(islikeBoolean)
            }
        })

    }

    fun setDeniedUserLike(isLike: Boolean, id: String){
        val user = FirebaseTools.getUser()
        val ref = FirebaseTools.getFireBaseRef(Constans.URL_FOR_iMAGE).child(id)
        ref.runTransaction(object : Transaction.Handler {
            override fun onComplete(p0: DatabaseError?, p1: Boolean, p2: DataSnapshot?) {

            }

            @SuppressLint("RestrictedApi")
            override fun doTransaction(mutableData: MutableData): Transaction.Result {
                var data = mutableData.getValue(ImageUrlJava::class.java)
                if (isLike) {
                    mutableData.child("likes").value = data?.likes!! - 1
                    mutableData.child("wholiked").child(user?.uid.toString()).value = null

                } else {
                    mutableData.child("likes").value = data?.likes!! + 1
                    mutableData.child("wholiked").child(user?.uid.toString()).value = user?.uid.toString()
                }

                return Transaction.success(mutableData)
            }
        }


        )
    }
}