package com.glasha.glashawallpaper.activities.mainActivity.fragments

import android.content.Intent
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.activities.fullImageActivity.FullImageActivity
import com.glasha.glashawallpaper.utils.base.BaseFragment
import com.glasha.glashawallpaper.activities.mainActivity.ViewModelMainActivity
import com.glasha.glashawallpaper.adapters.adapterMainRv.AdapterMainRV
import com.glasha.glashawallpaper.adapters.adapterMainRv.OnClickImage
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.glasha.glashawallpaper.databinding.FragmentImagesAllBinding
import com.glasha.glashawallpaper.utils.Constans

class LikedImagesFragment : BaseFragment<FragmentImagesAllBinding>(),
    OnClickImage {


    //private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var list: ArrayList<ImageUrlJava>

    override fun getLayout() = R.layout.fragment_images_all

    override fun setupBinding(binding: FragmentImagesAllBinding) {

        list = arrayListOf()
        viewAdapter = AdapterMainRV(list, this,context!!)
        viewManager = GridLayoutManager(this.context!!, 3) as RecyclerView.LayoutManager
        binding.mainRv.apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
        var viewModel = activity?.let { ViewModelProviders.of(it).get(ViewModelMainActivity::class.java) }
        var data = viewModel?.likedImages
        viewModel?.let { onError(it) }
        viewModel?.let { noNetwork(it) }
        data?.observe(this, Observer {
            viewAdapter = AdapterMainRV(it, this,context!!)
            binding.mainRv.apply {
                adapter = viewAdapter
            }
        })
    }

    private fun onError(viewModel: ViewModelMainActivity) {
        viewModel.onFailureLikedImages.observe(this, Observer {
            Toast.makeText(activity, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun noNetwork(viewModel: ViewModelMainActivity) {
        viewModel.noNetworkLikedImages.observe(this, Observer {
            Toast.makeText(activity, "Нет соединения с сервером", Toast.LENGTH_LONG).show()
        })
    }

    override fun onClick(item: ImageUrlJava) {
        var intent = Intent(activity, FullImageActivity::class.java)
        intent.putExtra(Constans.PUT_EXTRA_URL_IMAGE, item.url.toString())
        intent.putExtra(Constans.PUT_EXTRA_URL_PATH, item.path.toString())
        intent.putExtra(Constans.PUT_EXTRA_ID_IMAGE, item.id.toString())
        startActivity(intent)
    }
}