package com.glasha.glashawallpaper.activities.imageSortCategoryActivity.interfaces

import com.glasha.glashawallpaper.models.ImageUrlJava

interface CallBackOnGetSortedByCategoryImages {
    fun onSucces(list:ArrayList<ImageUrlJava>)
    fun noNetwork()
}