package com.glasha.glashawallpaper.activities.settingsAccauntActivity

import android.util.Log
import com.glasha.glashawallpaper.activities.settingsAccauntActivity.interfaces.CallbackGetUser
import com.glasha.glashawallpaper.models.User
import com.glasha.glashawallpaper.utils.FirebaseTools
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class RepositorySettingsAccauntActivity {
    fun getInfoUser(callbak:CallbackGetUser) {
        val user = FirebaseTools.getUser()
        var id = user?.uid
        var myRef = FirebaseTools.getFireBaseRef("User")
        myRef.child(id.toString()).addListenerForSingleValueEvent(object : ValueEventListener{
            override fun onCancelled(snapShot: DatabaseError) {


            }

            override fun onDataChange(snapShot: DataSnapshot) {
                if (snapShot.exists()) {
                    Log.d("cfc","ok")
                    var user = snapShot.getValue(User::class.java)
                    user?.let { callbak.getUser(it)
                        Log.d("cfc",user.email)
                    }?: run {
                       // Toast.makeText(context,"Что-то пошло не так, попробуйте позже", Toast.LENGTH_LONG).show()

                    }
                }
            }
        })
    }
}