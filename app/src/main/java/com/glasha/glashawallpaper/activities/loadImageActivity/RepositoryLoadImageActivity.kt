package com.glasha.glashawallpaper.activities.loadImageActivity

import android.net.Uri
import com.glasha.glashawallpaper.activities.loadImageActivity.interfaces.callbacks.CallBackOnLoadedImage
import com.glasha.glashawallpaper.models.Category
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.glasha.glashawallpaper.utils.Constans.Companion.URL_FOR_iMAGE
import com.glasha.glashawallpaper.utils.FirebaseTools
import com.glasha.glashawallpaper.utils.Utils.Companion.convertData
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask

class RepositoryLoadImageActivity {
     var listener:CallBackOnLoadedImage?=null


    fun setOnLoadedImage(listener: CallBackOnLoadedImage){
        this.listener = listener
    }
    fun loadImageToStorage(
        uriToLoad: Uri,
        path: String,
        hashTag: String,
        cat: Category
    ) {
        var alphaOnly = uriToLoad.toString().replace("/", "")
        alphaOnly = alphaOnly.replace(":", "")
        val riversRef = FirebaseTools.getFBStorageInstance().child("allImages/$alphaOnly")
        val uploadTask = riversRef.putFile(uriToLoad)
        uploadTask.addOnSuccessListener {
            getUrlLoadedImage(uploadTask, riversRef,path,hashTag,cat)


        }.addOnFailureListener {
            it.message?.let { it1 -> listener?.onFailure(it1) }

        }


    }

    fun getUrlLoadedImage(uploadTask: UploadTask, riversRef: StorageReference,path: String,hashTag: String,category: Category) {
        uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    it.message?.let { it1 -> listener?.onFailure(it1) }
                    throw it
                }
            }
            return@Continuation riversRef.downloadUrl
        }).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUri = task.result

                val user = FirebaseTools.getUser()
                var id = user?.uid
                //todo callback on update FB wallpaper

                updateDBWallpaper(id!!,downloadUri.toString(),path,hashTag,category)


            } else {
                task.exception?.message?.let { listener?.onFailure(it) }

            }
        }
    }

    fun updateDBWallpaper(
        idUser: String,
        urlForImage: String,
        path: String,
        hashTag: String,
        category: Category ) {

        var myRef= FirebaseTools.getFireBaseRef(URL_FOR_iMAGE)
        //val sett = Settings(context)
        val ref=myRef.push()

        val time = convertData()
        val data= ImageUrlJava(urlForImage,time,path,category.id,0,idUser,hashTag)
        ref.setValue(data)
        ref.child("wholiked").child("idusernotnull").setValue("iduser123")
        listener?.onLoaded()

    }

}