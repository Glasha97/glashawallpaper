package com.glasha.glashawallpaper.activities.logInActivity

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.MenuItem
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.databinding.FragmentInLogBinding
import com.glasha.glashawallpaper.utils.Constans
import com.glasha.glashawallpaper.utils.FirebaseTools
import com.glasha.glashawallpaper.utils.GoogleAuth
import com.glasha.glashawallpaper.utils.base.BaseActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import kotlinx.android.synthetic.main.fragment_in_log.*

class LogInActivity : BaseActivity<FragmentInLogBinding>() {


    private lateinit var googleSignInClient: GoogleSignInClient
    private val googleAuth = GoogleAuth(
        this, this, FirebaseTools.getAuthInstance()
    )

    override fun getLayout() = R.layout.fragment_in_log

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun buildToolBar() = binding.toolbarLogIn as Toolbar

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setupBinding(binding: FragmentInLogBinding) {
        showArrowBackPress()
        binding.googleAuth=googleAuth
        googleSignInClient = googleAuth.builtAuth()

        signup_google.setOnClickListener {
            //todo toolbar on
            googleAuth.signIn(googleSignInClient)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constans.START_ACTIVITY_RESULT_LOGIN_GOOGLE) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                account?.let {
                    googleAuth.firebaseAuthWithGoogle(it,pb_log_in)
                }
            } catch (e: ApiException) {
                Log.w("cfc", "Google sign in failed", e)
                pb_log_in.visibility = ProgressBar.GONE
            }
        } else {
            pb_log_in.visibility = ProgressBar.GONE

        }
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}