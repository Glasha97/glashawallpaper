package com.glasha.glashawallpaper.activities.loadImageActivity

import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.utils.base.BaseActivity
import com.glasha.glashawallpaper.adapters.AdapterCategorySpinner
import com.glasha.glashawallpaper.models.Category
import com.glasha.glashawallpaper.databinding.ActivityLoadImageBinding
import com.glasha.glashawallpaper.utils.Constans
import com.glasha.glashawallpaper.utils.PhotoUtils
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_load_image.*
import java.io.BufferedInputStream
import java.io.File
import java.io.IOException

class LoadImageActivity : BaseActivity<ActivityLoadImageBinding>(), AdapterView.OnItemSelectedListener,
    GetAllCategories.CallbackGetCategories {

    private val provider = GetAllCategories(this)
    private lateinit var mStorageRef: StorageReference
    private var uriToLoad = Uri.parse("android.resource://your.package.here/drawable/gallery.png")
    private lateinit var list: ArrayList<Category>
    private lateinit var cat: Category
    private lateinit var viewModelLoadImageActivity:ViewModelLoadImageActivity
    private var photoUtils=PhotoUtils(this,this)

    override fun getLayout() = R.layout.activity_load_image

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun buildToolBar() = binding.loadImageToolbar as Toolbar


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun setupBinding(binding: ActivityLoadImageBinding) {
        showArrowBackPress()
        viewModelLoadImageActivity = ViewModelProviders.of(this).get(ViewModelLoadImageActivity::class.java)
        viewModelLoadImageActivity.toastMessage.observe(this, Observer { res ->
            if (res != null) {
                val message = res.format(this@LoadImageActivity)
                Toast.makeText(this@LoadImageActivity, message, Toast.LENGTH_LONG).show()
            }
        })

        binding.viewModel=viewModelLoadImageActivity
        mStorageRef = FirebaseStorage.getInstance().reference
        provider.get()
        binding.checkImage.setOnClickListener {
            com.glasha.glashawallpaper.utils.Dexter.isExternalGranted(this, object :
                com.glasha.glashawallpaper.utils.Dexter.CallBackOnPermission {
                override fun onGranted() {
                    try {
                        val photoFile: File? = photoUtils.createTempImageFile(this@LoadImageActivity)
                        val photoURI = FileProvider.getUriForFile(
                            this@LoadImageActivity,
                            "com.glasha.glashawallpaper.fileprovider",
                            photoFile!!.absoluteFile
                        )
                        startActivityForResult(
                            photoUtils.intentToPhoto(this@LoadImageActivity, photoURI),
                            Constans.START_ACTIVITY_RESULT_CHOOSE_PHOTO
                        )
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

                override fun onDenied() {
                    Toast.makeText(
                        this@LoadImageActivity,
                        "Для загрузки изображения необходим доступ к памяти",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

            })


        }
        load_image_btn.setOnClickListener {
            if (uriToLoad == Uri.parse("android.resource://your.package.here/drawable/gallery.png")) {
                Toast.makeText(this, "Выберите изображение", Toast.LENGTH_LONG).show()
            } else {
                val hashTag = editText_hashtag.text.toString()

                viewModelLoadImageActivity.loadImage(uriToLoad, uriToLoad.toString(), hashTag, cat)

            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constans.START_ACTIVITY_RESULT_CHOOSE_PHOTO) {
            if (data?.data != null) {
                photoUtils.showImage(data!!, check_image)
                val inputStream = this.contentResolver.openInputStream(data.data)
                val buffInputStream = BufferedInputStream(inputStream)
                val bitmap = BitmapFactory.decodeStream(buffInputStream)
                uriToLoad = photoUtils.getImageUri(this@LoadImageActivity, bitmap)
            }
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        cat = list[p2]
    }

    override fun getCategories(result: ArrayList<Category>) {
        list = result
        val adapter = AdapterCategorySpinner(this, R.layout.item_adapter_spinner, R.id.text_spinner, list)
        spinner_categories.onItemSelectedListener = this
        spinner_categories.adapter = adapter
    }


}