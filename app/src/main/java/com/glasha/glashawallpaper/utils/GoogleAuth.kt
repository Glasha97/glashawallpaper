package com.glasha.glashawallpaper.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.databinding.ObservableBoolean
import com.glasha.glashawallpaper.activities.settingsAccauntActivity.SettingsAccauntActivity
import com.glasha.glashawallpaper.models.User
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.database.FirebaseDatabase

class GoogleAuth(private val context: Context, private val activity: Activity, private val auth: FirebaseAuth) {
    var showPB = ObservableBoolean(false)
    var showPBSettingsAccaunt = ObservableBoolean(false)


    fun builtAuth(): GoogleSignInClient {
        var googleSignInClient: GoogleSignInClient
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(Constans.TOKEN_GOOGLE_AUTH)
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(context, gso)
        return googleSignInClient
    }

    fun signIn(googleSignInClient: GoogleSignInClient) {
        showPB.set(true)
        val signInIntent = googleSignInClient.signInIntent
        activity.startActivityForResult(signInIntent, Constans.START_ACTIVITY_RESULT_LOGIN_GOOGLE)

    }

    fun firebaseAuthWithGoogle(acct: GoogleSignInAccount, pb: ProgressBar) {
        Log.d("cfc", "firebaseAuthWithGoogle:" + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    val user = auth.currentUser
                    var userData = User(user!!.uid, user.displayName, user.email, user.photoUrl.toString())
                    registrateOrUpdateUser(userData)
                    val intent = Intent(context, SettingsAccauntActivity::class.java)
                    activity.startActivity(intent)
                    showPB.set(false)

                } else {
                    showPB.set(false)

                }

            }
    }

    fun registrateOrUpdateUser(user: User) {
        showPBSettingsAccaunt.set(true)
        //todo add callback
        var myRef = FirebaseTools.getFireBaseRef("User")

        Log.d("cfc", "kjh ${showPBSettingsAccaunt.get().toString()}")

        if (user.urlImage == "") {
            //   callback.onReadyREgistrate()
            myRef.child(user.id.toString()).child("name").setValue(user.name)
            showPBSettingsAccaunt.set(false)
            Log.d("cfc", "kjh ${showPBSettingsAccaunt.get().toString()}")


        } else {
            myRef.child(user.id.toString()).child("id").setValue(user.id)
            myRef.child(user.id.toString()).child("email").setValue(user.email)
            myRef.child(user.id.toString()).child("name").setValue(user.name)
            myRef.child(user.id.toString()).child("urlImage").setValue(user.urlImage)
            showPBSettingsAccaunt.set(false)
            Log.d("cfc", "kjh $showPBSettingsAccaunt")


        }

    }


}