package com.glasha.glashawallpaper.utils

import android.app.Activity
import android.os.Build
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import java.io.File
import java.io.IOException

class Dexter {
    companion object {


        fun isExternalGranted(
            activity: Activity, callBackOnPermission: CallBackOnPermission
        ) {

            Dexter.withActivity(activity)
                .withPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object : PermissionListener {
                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest?,
                        token: PermissionToken?
                    ) {
                        token?.continuePermissionRequest()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                        callBackOnPermission.onDenied()

                    }

                    @RequiresApi(Build.VERSION_CODES.N)
                    override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                        callBackOnPermission.onGranted()

                    }
                }).check()
        }
    }

    interface CallBackOnPermission {
        fun onGranted()
        fun onDenied()
    }
}