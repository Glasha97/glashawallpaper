package com.glasha.glashawallpaper.utils.base

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BaseFragment <B : ViewDataBinding> : androidx.fragment.app.Fragment() {

    abstract fun getLayout(): Int
    abstract fun setupBinding(binding: B)
    protected lateinit var binding: B
    private var progressBar: ProgressBar? = null



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, getLayout(), container, false)
        setupBinding(binding)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)

    fun bindProgressBar(progressBar: ProgressBar) {
        this.progressBar = progressBar
    }

    fun startProgress() {
        progressBar?.let {
            it.visibility = ProgressBar.VISIBLE
        }
    }

    fun stopProgress() {
        progressBar?.let {
            it.visibility = ProgressBar.GONE
        }
    }



}