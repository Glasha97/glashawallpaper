package com.glasha.glashawallpaper.utils

import android.app.TimePickerDialog
import android.content.Context
import android.view.View
import androidx.appcompat.widget.AppCompatEditText

class DateEditPicker(private val _context: Context, private val editText: AppCompatEditText,private val callBack: CallbackTimePickerOnChanged) : View.OnClickListener {
    private var _minutes: Int = 0
    private var _hour: Int = 0


    init {
        editText.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        val timer = TimePickerDialog(_context, listener, 0, 0, true)
        timer.show()
    }

    private fun updateDisplay() {
        var hourStr: String
        var minStr: String
        if (_hour.toString().length == 1) {
            hourStr = "0$_hour"
        }else{
            hourStr = _hour.toString()

        }
        if (_minutes.toString().length == 1) {
            minStr = "0$_minutes"
        }else{
            minStr = _minutes.toString()

        }

        editText.setText(
            StringBuilder()
                // Month is 0 based so add 1
                .append("Интервал").append(" ").append(hourStr).append(":").append(minStr)
        )
        callBack.onChanged()
    }

    private val listener = TimePickerDialog.OnTimeSetListener { p0, hour, min ->
        _minutes = min
        _hour = hour
        updateDisplay()
    }
}
