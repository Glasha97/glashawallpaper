package com.glasha.glashawallpaper.utils


interface CallbackTimePickerOnChanged {
    fun onChanged()
}