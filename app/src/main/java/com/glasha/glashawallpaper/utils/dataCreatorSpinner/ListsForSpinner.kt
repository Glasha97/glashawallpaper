package com.glasha.glashawallpaper.utils.dataCreatorSpinner

import android.content.Context
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.utils.Constans

class ListsForSpinner {
    companion object{
        fun addListWhichWallpaper(context: Context):ArrayList<String>{
            val list=ArrayList<String>()
            list.add(context.getString(R.string.all_images))
            list.add(context.getString(R.string.liked))
            return list
        }

        fun addListWhereWallpaper():ArrayList<String>{
            val list=ArrayList<String>()
            list.add(Constans.DEFAULT_WORK_MANAGER_WHERE_WALLPAPER_RUS)
            list.add(Constans.WORK_MANAGER_WHERE_WALLPAPER_LOCKSCREEN_RUS)
            list.add(Constans.WORK_MANAGER_WHERE_WALLPAPER_ALL_RUS)
            return list
        }

    }
}