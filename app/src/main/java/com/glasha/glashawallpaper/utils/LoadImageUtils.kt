package com.glasha.glashawallpaper.utils

import android.app.Activity
import android.app.WallpaperManager
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.provider.MediaStore
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.glasha.glashawallpaper.R
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LoadImageUtils(private val context: Context) {
    private var activity: Activity? = null

    constructor(context: Context, activity: Activity) : this(context){
        this.activity = activity

    }

    fun loadImageToStorage(url: String) {
        com.glasha.glashawallpaper.utils.Dexter.isExternalGranted(activity!!,object :
            com.glasha.glashawallpaper.utils.Dexter.CallBackOnPermission{
            override fun onGranted() {
                Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                        ) {
                            MediaStore.Images.Media.insertImage(context.contentResolver, resource, "", "")

                        }

                        override fun onLoadCleared(placeholder: Drawable?) {

                        }
                    })
            }

            override fun onDenied() {
                Toast.makeText(context, context.getString(R.string.needpermExternal), Toast.LENGTH_SHORT).show()

            }
        })
    }
    fun loadAndChangeWallpaper(url: String, kind: Int) {
        com.glasha.glashawallpaper.utils.Dexter.isExternalGranted(activity!!,object :
            com.glasha.glashawallpaper.utils.Dexter.CallBackOnPermission{
            override fun onGranted() {
                Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .into(object : CustomTarget<Bitmap>() {
                        override fun onLoadFailed(errorDrawable: Drawable?) {

                        }

                        @RequiresApi(Build.VERSION_CODES.N)
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                        ) {

                            MediaStore.Images.Media.insertImage(context.contentResolver, resource, "", "")
                            GlobalScope.launch {
                                var wallpaper: WallpaperManager = WallpaperManager.getInstance(context)
                                wallpaper.setBitmap(resource, null, true, kind)
                            }
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {

                        }
                    })
            }
            override fun onDenied() {
                Toast.makeText(context, context.getString(R.string.needpermExternal), Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun loadAndChangeWallpaper(url: String) {

        com.glasha.glashawallpaper.utils.Dexter.isExternalGranted(activity!!,object :
            com.glasha.glashawallpaper.utils.Dexter.CallBackOnPermission{
            override fun onGranted() {
                Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .into(object : CustomTarget<Bitmap>() {
                        @RequiresApi(Build.VERSION_CODES.N)
                        override fun onResourceReady(
                            resource: Bitmap,
                            transition: com.bumptech.glide.request.transition.Transition<in Bitmap>?
                        ) {
                            MediaStore.Images.Media.insertImage(context.contentResolver, resource, "", "")
                            GlobalScope.launch {
                                var wallpaper: WallpaperManager = WallpaperManager.getInstance(context)
                                wallpaper.setBitmap(resource, null, true)
                            }
                        }

                        override fun onLoadCleared(placeholder: Drawable?) {

                        }
                    })
            }
            override fun onDenied() {
                Toast.makeText(context, context.getString(R.string.needpermExternal), Toast.LENGTH_SHORT).show()

            }
        })

    }
}