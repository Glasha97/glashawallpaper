package com.glasha.glashawallpaper.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Parcelable
import android.provider.MediaStore
import android.widget.ImageView
import android.widget.Toast
import com.glasha.glashawallpaper.R
import com.squareup.picasso.Picasso
import java.io.BufferedInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*

class PhotoUtils(private val context: Context, private val activity: Activity) {
    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    fun changePhotoUser(data: Intent, imageAcc: ImageView): Uri {
        var uri = Uri.parse("")
        com.glasha.glashawallpaper.utils.Dexter.isExternalGranted(activity, object :
            com.glasha.glashawallpaper.utils.Dexter.CallBackOnPermission {
            override fun onGranted() {
                val inputStream = context.contentResolver.openInputStream(data!!.data)
                val buffInputStream = BufferedInputStream(inputStream)
                val bitmap = BitmapFactory.decodeStream(buffInputStream)
                uri = getImageUri(
                    context
                    , bitmap
                )
                Picasso.with(context)
                    .load(uri)
                    .fit().centerCrop()
                    .into(imageAcc)
                // val mAuth = FirebaseAuth.getInstance()

                //  val user = mAuth.currentUser
//                    //todo update userInfo
////                    val getInfoUserProvider = GetInfoUserProvider(context, this)
////                    var database = FirebaseDatabase.getInstance()
////                    var myRef = database.getReference("User")
////                    myRef.child(user!!.uid).addListenerForSingleValueEvent(getInfoUserProvider)
            }

            override fun onDenied() {
                Toast.makeText(context, context.getString(R.string.needpermExternal), Toast.LENGTH_SHORT).show()

            }

        })

        return uri
    }

    fun intentToPhoto(context: Context, photoURI: Uri): Intent? {
        var intentList: MutableList<Intent> = ArrayList()
        var chooserIntent: Intent? = null
        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
        intentList = addIntentsToList(context, intentList, pickIntent).toMutableList()
        intentList = addIntentsToList(context, intentList, takePhotoIntent).toMutableList()
        if (intentList.isNotEmpty()) {

            chooserIntent = Intent.createChooser(intentList.removeAt(intentList.size - 1), "Choose your image source")
            chooserIntent?.let { it.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toTypedArray<Parcelable>()) }
        }
        return chooserIntent
    }

    private fun addIntentsToList(context: Context, list: MutableList<Intent>, intent: Intent): List<Intent> {
        val resInfo = context.packageManager.queryIntentActivities(intent, 0)
        for (resolveInfo in resInfo) {
            val packageName = resolveInfo.activityInfo.packageName
            val targetedIntent = Intent(intent)
            targetedIntent.setPackage(packageName)
            list.add(targetedIntent)
        }
        return list
    }

    fun createTempImageFile(context: Context): File {


        val timeStamp = java.lang.Long.toString(System.currentTimeMillis())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = File(context.filesDir, "photos")
        if (!storageDir.exists()) storageDir.mkdir()
        return File.createTempFile(imageFileName, ".jpg", storageDir)
    }

    fun showImage(data: Intent, imageAcc: ImageView) {
        Dexter.isExternalGranted(activity, object :
            com.glasha.glashawallpaper.utils.Dexter.CallBackOnPermission {
            override fun onGranted() {
                val inputStream = context.contentResolver.openInputStream(data!!.data)
                val buffInputStream = BufferedInputStream(inputStream)
                val bitmap = BitmapFactory.decodeStream(buffInputStream)
                val uri = getImageUri(
                    context
                    , bitmap
                )
                Picasso.with(context)
                    .load(uri)
                    .fit().centerCrop()
                    .into(imageAcc)
            }

            override fun onDenied() {
                Toast.makeText(context, context.getString(R.string.needpermExternal), Toast.LENGTH_SHORT).show()
            }

        })

    }

}