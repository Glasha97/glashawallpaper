package com.glasha.glashawallpaper.utils.base

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding


abstract class BaseActivity<B : ViewDataBinding> : AppCompatActivity() {

    abstract fun getLayout(): Int
    abstract fun buildToolBar(): androidx.appcompat.widget.Toolbar
    abstract fun setupBinding(binding: B)
    protected lateinit var binding: B
    private var progressBar: ProgressBar? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, getLayout())
        setupBinding(binding)
    }

    @SuppressLint("NewApi")
    fun setTitleScreen(title: String) {
        buildToolBar()?.run {
            this.title = title
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun showArrowBackPress() {
        setSupportActionBar(buildToolBar())
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }






    fun bindProgressBar(progressBar: ProgressBar) {
        this.progressBar = progressBar
    }

    fun startProgress() {
        progressBar?.let {
            it.visibility = ProgressBar.VISIBLE
        }
    }

    fun stopProgress() {
        progressBar?.let {
            it.visibility = ProgressBar.GONE
        }
    }


    fun hideKeyboard() {
        this?.let {
            val imm = it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            val v = this?.currentFocus
            v?.let {
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                v.clearFocus()
            }
        }
    }
}