package com.glasha.glashawallpaper.utils

import android.app.Activity
import android.util.Log
import android.widget.Toast
import com.glasha.glashawallpaper.R
import java.text.SimpleDateFormat
import java.util.*

class Utils {



    companion object {
        fun convertData(): Long {

            val sdf = SimpleDateFormat("yyyyMMddHHmmss")
            val currentDate = sdf.format(Date())
            return currentDate.toLong()
        }
        fun convertTimeToMinute(time: String): Long {
            Log.d("cfc", time)
            if (time != "") {
                val hours = time.dropLast(3)
                val minutes = time.drop(3)
                return hours.toLong() * 60 + minutes.toLong()
            } else {
                return 120
            }

        }


        fun toastNoNetwork(activity: Activity) {
            Toast.makeText(activity,activity.getString(R.string.noNetwork),Toast.LENGTH_LONG).show()

        }
    }
}