package com.glasha.glashawallpaper.utils

import android.app.Activity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class FirebaseTools {
    companion object {

        fun getUser(): FirebaseUser? {
            val mAuth = getAuthInstance()
            val user = mAuth?.currentUser
            return user
        }

        fun getUIDUser(): String {
            val user = getUser()
            return user?.uid.toString()


        }

        fun getAuthInstance(): FirebaseAuth {
            return FirebaseAuth.getInstance()

        }

        fun getFireBaseRef(child: String): DatabaseReference {
            var firebaseDatabase = FirebaseDatabase.getInstance()
            return firebaseDatabase!!.getReference(child)
        }

        fun getFireBaseRef(): DatabaseReference {
            var firebaseDatabase = FirebaseDatabase.getInstance()
            return firebaseDatabase!!.reference
        }


        fun isConnected(): Boolean {
            var connected = true
            val connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected")
            connectedRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    connected = snapshot.getValue(Boolean::class.java) ?: false

                }

                override fun onCancelled(error: DatabaseError) {
                    connected = false
                }
            })
            return connected
        }

        fun logout() {
            FirebaseAuth.getInstance()?.let {
                FirebaseAuth.getInstance().signOut()
            }

        }

        fun googleLogout(activity: Activity) {
            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("619890174192-j2m1lkv071tg6f7t9h4uil01264p5h8q.apps.googleusercontent.com")
                .requestEmail()
                .build()

            var googleSignInClient = GoogleSignIn.getClient(activity, gso)
            googleSignInClient.signOut().addOnSuccessListener {
                activity.finish()
            }
        }


        fun getFBStorageInstance(): StorageReference {
            return FirebaseStorage.getInstance().reference

        }
    }

}