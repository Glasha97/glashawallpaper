package com.glasha.glashawallpaper.models;

import android.widget.ImageView;
import androidx.databinding.BindingAdapter;
import com.glasha.glashawallpaper.R;
import com.google.firebase.database.IgnoreExtraProperties;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

@IgnoreExtraProperties
public class ImageUrlJava {
    private String id;
    private String url;
    private Long time;
    private String path;
    private long categoryId;
    private long likes;
    private HashMap<String, String> wholiked;
    private String idUser;
    private String hashTag;

    public ImageUrlJava(String url, Long time, String path, long categoryId, long likes, HashMap<String, String> wholiked, String idUser, String hashTag) {
        this.url = url;
        this.time = time;
        this.path = path;
        this.categoryId = categoryId;
        this.likes = likes;
        this.wholiked = wholiked;
        this.idUser = idUser;
        this.hashTag = hashTag;
    }

    public ImageUrlJava(String url, Long time, String path, long categoryId, long likes, String idUser, String hashTag) {
        this.url = url;
        this.time = time;
        this.path = path;
        this.categoryId = categoryId;
        this.likes = likes;
        this.idUser = idUser;
        this.hashTag = hashTag;
    }

    public ImageUrlJava(String id, String url, Long time, String path, long categoryId, long likes, HashMap<String, String> wholiked, String idUser, String hashTag) {
        this.id = id;
        this.url = url;
        this.time = time;
        this.path = path;
        this.categoryId = categoryId;
        this.likes = likes;
        this.wholiked = wholiked;
        this.idUser = idUser;
        this.hashTag = hashTag;
    }
    public ImageUrlJava(String id, String url, Long time, String path, long categoryId, long likes) {
        this.id = id;
        this.url = url;
        this.time = time;
        this.path = path;
        this.categoryId = categoryId;
        this.likes = likes;
    }

    public ImageUrlJava() {
    }

    public String getHashTag() {
        return hashTag;
    }

    public void setHashTag(String hashTag) {
        this.hashTag = hashTag;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public long getLikes() {
        return likes;
    }

    public void setLikes(long likes) {
        this.likes = likes;
    }

    public HashMap<String, String> getWholiked() {
        return wholiked;
    }

    public void setWholiked(HashMap<String, String> wholiked) {
        this.wholiked = wholiked;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }



        @BindingAdapter("bind:imageUrl")
        public static void loadImage(ImageView imageView, String v) {
           // Picasso.with(imageView.getContext()).load(v).into(imageView);

            Picasso.with(imageView.getContext())
                 .load(v)
                 .placeholder(R.drawable.gallery)
                 .error(R.drawable.gallery)
                 .fit().centerCrop()
                 .into(imageView);
        }

}
