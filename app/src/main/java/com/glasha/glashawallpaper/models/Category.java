package com.glasha.glashawallpaper.models;

import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import com.glasha.glashawallpaper.R;
import com.google.firebase.database.*;
import com.squareup.picasso.Picasso;

public class Category {
    private long id;
    private String name;
    private long count;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Category() {
    }

    public Category(long id, String name, long count, String url) {
        this.id = id;
        this.name = name;
        this.count = count;
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @BindingAdapter("bind:imageCategory")
    public static void loadImageCategory(ImageView imageView, String v) {
        // Picasso.with(imageView.getContext()).load(v).into(imageView);

        Picasso.with(imageView.getContext())
                .load(v)
                .placeholder(R.drawable.gallery)
                .error(R.drawable.gallery)
                .fit()
                .into(imageView);
    }

    @BindingAdapter("bind:CountImagesInCategory")
    public static void count(final TextView textView, final Long id){
        FirebaseDatabase firebaseDatabase=FirebaseDatabase.getInstance();
        DatabaseReference reference=firebaseDatabase.getReference("urlForImages");
        final Long[] count = {0L};
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for(DataSnapshot shot: dataSnapshot.getChildren()){
                        ImageUrlJava url=shot.getValue(ImageUrlJava.class);

                        if(url!=null){
                            if (id == url.getCategoryId()) {
                                count[0]++;
                            }
                        }


                    }
                    textView.setText(count[0].toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
//    fun count(tv: TextView, id: Long) {
//        var firebaseDatabase = FirebaseDatabase.getInstance()
//        var databaseReference = firebaseDatabase!!.getReference("urlForImages")
//        var count = 0
//        databaseReference!!.addListenerForSingleValueEvent(object : ValueEventListener {
//            override fun onCancelled(p0: DatabaseError) {
//            }
//
//            override fun onDataChange(dataSnapshot: DataSnapshot) {
//                if (dataSnapshot.exists()) {
//                    for (shot in dataSnapshot.children) {
//
//                        var url = shot.getValue(ImageUrlJava::class.java)
//                        url?.let {
//                            if (id == it.categoryId) {
//                                count++
//                            }
//                        }
//                    }
//                }
//                tv.text = "$count"
//                // countTv?.text = " $count"
//            }
//        })
//    }
//
//



}
