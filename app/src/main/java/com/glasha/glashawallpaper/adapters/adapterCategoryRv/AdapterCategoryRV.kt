package com.glasha.glashawallpaper.adapters.adapterCategoryRv

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.databinding.ItemRvCategoryBinding
import com.glasha.glashawallpaper.models.Category
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso

class AdapterCategoryRV(
    private val _list: ArrayList<Category>,
    private val listener: OnClickCategory,
    private val context: Context


) : RecyclerView.Adapter<AdapterCategoryRV.CategoryVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CategoryVH(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_rv_category,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: CategoryVH, position: Int) {
        holder.bind(_list[position],listener,holder.adapterPosition,context)
    }

    override fun getItemCount(): Int {
        return _list.size
    }

    class CategoryVH(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val binding: ItemRvCategoryBinding = ItemRvCategoryBinding.bind(itemView)

        fun bind(data:Category, listener: OnClickCategory, position: Int,context:Context) {
            binding.item=data
            binding.position=position
            binding.listener=listener
            binding.nameCategory.text=data.name
            Picasso.with(context)
                .load(data.url)
                .placeholder(R.drawable.gallery)
                .error(R.drawable.gallery)
                .fit()
                .into(binding.ivCategory)

        }
    }
}