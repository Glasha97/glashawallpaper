package com.glasha.glashawallpaper.adapters.adapterCategoryRv

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso

class HandleCategoryRV {
    companion object {
        @BindingAdapter("bind:CountImagesInCategory")
        fun count(tv: TextView, id: Long) {
            var firebaseDatabase = FirebaseDatabase.getInstance()
            var databaseReference = firebaseDatabase!!.getReference("urlForImages")
            var count = 0
            databaseReference!!.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (dataSnapshot.exists()) {
                        for (shot in dataSnapshot.children) {

                            var url = shot.getValue(ImageUrlJava::class.java)
                            url?.let {
                                if (id == it.categoryId) {
                                    count++
                                }
                            }
                        }
                    }
                    tv.text = "$count"
                    // countTv?.text = " $count"
                }
            })
        }





    }
}