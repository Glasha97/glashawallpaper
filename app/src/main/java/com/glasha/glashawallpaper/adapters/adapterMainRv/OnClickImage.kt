package com.glasha.glashawallpaper.adapters.adapterMainRv

import com.glasha.glashawallpaper.models.ImageUrlJava

interface OnClickImage {
    fun onClick(item: ImageUrlJava)
}