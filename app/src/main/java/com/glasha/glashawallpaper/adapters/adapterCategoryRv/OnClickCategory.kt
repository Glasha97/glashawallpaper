package com.glasha.glashawallpaper.adapters.adapterCategoryRv

import com.glasha.glashawallpaper.models.Category

interface OnClickCategory {
    fun onClick(adapterPosition: Int, item: Category)
}