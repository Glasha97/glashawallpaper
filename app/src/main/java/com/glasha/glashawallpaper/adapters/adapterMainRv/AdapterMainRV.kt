package com.glasha.glashawallpaper.adapters.adapterMainRv

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.glasha.glashawallpaper.models.ImageUrlJava
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.databinding.ItemMainRvBinding
import com.squareup.picasso.Picasso
import android.R.attr.radius
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation


class AdapterMainRV(
    private val _list: ArrayList<ImageUrlJava>,
    private val listener: OnClickImage,
    private val context: Context


) : RecyclerView.Adapter<AdapterMainRV.MainVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MainVH(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_main_rv,
                parent,
                false
            )
        )


    override fun getItemCount() = _list.size

    override fun onBindViewHolder(p0: MainVH, p1: Int) {
        //    p0.bind( clickListener,_list[p1],context)
        val item = _list[p1]
        p0.binding.item = item
        p0.bind(item, listener, context)
    }

    class MainVH(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val binding: ItemMainRvBinding = ItemMainRvBinding.bind(itemView)

        fun bind(item: ImageUrlJava, listener: OnClickImage, context: Context) {
            binding.item = item
            val transformation = RoundedCornersTransformation(8, 0)

            binding.listener = listener
            Picasso.with(context)
                .load(item.url)
               // .transform(transformation)
                .placeholder(R.drawable.gallery)
                .error(R.drawable.gallery)
                .fit()
                .centerCrop()
                .into(binding.ivforrv)
        }

    }
}