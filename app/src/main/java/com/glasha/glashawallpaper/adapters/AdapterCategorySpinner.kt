package com.glasha.glashawallpaper.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.glasha.glashawallpaper.R
import com.glasha.glashawallpaper.models.Category





class AdapterCategorySpinner(context: Activity, resourceId: Int, textviewId: Int, list: ArrayList<Category>) :
    ArrayAdapter<Category>(context, resourceId, textviewId, list) {
    private var flater: LayoutInflater? = null

    init {
        flater = context.layoutInflater
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
    var convertView = convertView
    if (convertView == null) {
        convertView = flater?.inflate(R.layout.item_adapter_spinner, parent, false)
    }
    val rowItem = getItem(position)
    val txtTitle = convertView!!.findViewById<View>(R.id.text_spinner) as TextView
    txtTitle.text = rowItem!!.name
        return convertView
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView = convertView
        if (convertView == null) {
            convertView = flater?.inflate(R.layout.item_adapter_spinner, parent, false)
        }
        val rowItem = getItem(position)
        val txtTitle = convertView!!.findViewById<View>(R.id.text_spinner) as TextView
        txtTitle.text = rowItem!!.name

        return convertView
    }

}